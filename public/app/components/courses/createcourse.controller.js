(function() {
    'use strict';

    angular
        .module('coursesApp')
        .controller('CreateCourseCtrl', ['$scope', 'Course', CreateCourseCtrl]);

    function CreateCourseCtrl($scope, Course) {
        $scope.course = new Course();
        $scope.saveCourse = function(course) {
            $scope.errors = null;
            $scope.updating = true;
            course.$save(course)
            .catch(function(course) {
                $scope.errors = [note.data.error];
            }).finally(function() {
                $scope.updating = false;
            });
        };

    }
    
})();