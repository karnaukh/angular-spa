(function() {
    'use strict';

    angular
        .module('coursesApp')
        .controller('EditCourseCtrl', ['$scope', '$routeParams', 'Course', EditCourseCtrl]);

    function EditCourseCtrl($scope, $routeParams, Course) {
        Course.get({id: $routeParams.id}).$promise.then(function(course) {
            $scope.course = course;
            $scope.course.date = new Date(Number(course.date));
            $scope.course.duration = Number(course.duration);
        });
        
        $scope.updateCourse = function(course) {
            course.$update();
            console.log('asdfasd');
        };

        $scope.deleteCourse = function(course) {
            Course.$remove(course);
        };
    }
    
})();