(function() {
    'use strict';

    angular
        .module('coursesApp')
        .controller('RemoveCourseCtrl', ['$scope', 'Course', RemoveCourseCtrl]);

    function RemoveCourseCtrl($scope, Course) {
        $scope.removeCourse = function(course) {
            Course.$remove(course);
            console.log(course, 'course');
        };

    }
    
})();