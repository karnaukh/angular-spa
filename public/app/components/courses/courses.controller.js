(function() {
    'use strict';

    angular
        .module('coursesApp')
        .controller('CoursesCtrl', ['$scope', '$rootScope', 'Course', CoursesCtrl]);

    function CoursesCtrl($scope, $rootScope, Course) {
        $scope.courses = Course.query();
        $scope.removeCourse = function(course) {
            Course.$remove(course);
            console.log(course, 'course');
        };

    }
    
})();