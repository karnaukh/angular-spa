(function() {
    'use strict';

    angular
        .module('coursesApp', ['ngRoute', 'ngResource'])
        //.config(config)
        .controller('MainCtrl', function($rootScope /*, $location*/ ) {
            $rootScope.isLoggedIn = false;
            // if($rootScope.isLoggedIn) {
            //     $location.path('/courses');
            // }
        })
        
        .filter('prettyfyDuration', prettyfyDuration)
        .constant('credentials', {
            login: 'user',
            password: '111'
        })
        
        .controller('LoginCtrl', ['$scope', '$rootScope', 'credentials', '$location', LoginCtrl])
        



    // function config($routeProvider) {
    //     $routeProvider
    //         .when('/courses', {
    //             templateUrl: 'avengers.html',
    //             controller: 'Avengers',
    //             controllerAs: 'vm'
    //         });
    // }

    .config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

        $routeProvider
            .when('/', {
                // redirect to the notes index
                //redirectTo: '/login',
                templateUrl: '/app/components/login/login.html',
                controller: 'LoginCtrl'
            })

        .when('/login', {
            templateUrl: '/app/components/login/login.html',
            controller: 'LoginCtrl'
        })

        .when('/courses', {
            templateUrl: '/app/components/courses/all.html',
            controller: 'CoursesCtrl'
        })

        .when('/courses/new', {
            templateUrl: '/app/components/courses/add.html',
            controller: 'CreateCourseCtrl'
        })

        .when('/courses/:id', {
            templateUrl: '/app/components/courses/edit.html',
            controller: 'EditCourseCtrl'
        })

        .otherwise({ redirectTo: '/' });

    }]);

    function LoginCtrl($scope, $rootScope, credentials, $location) {
        $scope.showAlertMessage = false;
        $scope.pleaseLoginIsActive = false;
        $scope.pleasePasswordIsActive = false;
        $scope.verifyPassword = function() {
            $rootScope.isLoggedIn = $scope.login === credentials.login && $scope.password === credentials.password;
            $scope.pleaseLoginIsActive = !$scope.login;
            $scope.pleasePasswordIsActive = !$scope.password;
            if (!$scope.pleaseLoginIsActive && !$scope.pleasePasswordIsActive) {
                $scope.showAlertMessage = !$rootScope.isLoggedIn;
            }
            if($rootScope.isLoggedIn) {
                $location.path('/courses');
            }

        };
    }



    

    function prettyfyDuration() {
        return function(minutes, hoursFormat, minutesFormat) {
            var hours = parseInt(minutes / 60, 10);
            var minutesString = '';
            var hoursString = '';
            hoursFormat = hoursFormat || 'h';
            minutesFormat = minutesFormat || 'min';
            minutesString = minutes % 60 + minutesFormat;
            hoursString = hours + hoursFormat;
            return hours ? hoursString + ' ' + minutesString : minutesString;
        };
    }

    
})();