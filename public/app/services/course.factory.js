(function() {
    'use strict';

    // angular
    //     .module('coursesApp')
    //     .factory('coursesFactory', coursesFactory);

    // function coursesFactory() {
    //     var defaultCourses = [{
    //         'name': 'Course 1',
    //         'duration': '88',
    //         'date': '1455461271773',
    //         'description': 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat iure quisquam sequi, et beatae soluta, quis illo veniam veritatis, distinctio rerum modi maiores dolor maxime labore, deleniti ullam vero blanditiis!'
    //     }, {
    //         'name': 'Course 2',
    //         'duration': '25',
    //         'date': '1455634071773',
    //         'description': 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis repellat, esse rem! Ipsa enim quis delectus, quas ipsum excepturi explicabo quos, sunt cumque ea nihil qui, fugit molestiae eaque perferendis!'
    //     }];
    //     var savedCourses = localStorage.getItem('courses');
        
    //     return {
    //         getCourses: function() {
    //             return savedCourses ? JSON.parse(savedCourses) : defaultCourses;
    //         },
    //         getCourse: function(index) {
    //             return this.getCourses()[index];
    //         }
    //     };
    // }
    angular
        .module('coursesApp')
        .factory('Course', ['$resource', Course]);

    function Course($resource) {
        return $resource('/courses/:id', {}, {
            update: {
                method: 'PUT'
            }
        });        
    }
})();