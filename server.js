'use strict';

const express = require('express');
const app = express();

const port = process.env.PORT || 3000;

const authentication = require('./app/middleware.js').authenticate;
const bodyParser = require('body-parser');

var courses = [{
            id: 1,
            'name': 'Course 1',
            'duration': '88',
            'date': '1455461271773',
            'description': 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat iure quisquam sequi, et beatae soluta, quis illo veniam veritatis, distinctio rerum modi maiores dolor maxime labore, deleniti ullam vero blanditiis!'
        }, {
            id: 2,
            'name': 'Course 2',
            'duration': '25',
            'date': '1455634071773',
            'description': 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis repellat, esse rem! Ipsa enim quis delectus, quas ipsum excepturi explicabo quos, sunt cumque ea nihil qui, fugit molestiae eaque perferendis!'
        }];
//let courses = [];
let courseLastId = courses.length ? courses[courses.length-1].id : 0;

//app.use(authentication);

app.use(bodyParser.json());

// app.get('/app/components/courses/all.html', authentication, (req, res) => {
//     res.send('You need login');
// });

app.get('/courses', (req, res) => {
    res.json(courses);
});

app.get('/courses/:id', (req, res) => {
    let courseId = Number(req.params.id);
    let matchedCourse = courses.filter((course) => course.id === courseId)[0];

    matchedCourse ? res.json(matchedCourse) : res.status(404).send({'error': 'no course found'});

    // res.status(404).send();
    //res.send('requested id: ' + req.params.id);
});

app.post('/courses', (req, res) => {
    let body = req.body;
    
    let courseNextId = ++courseLastId;

    console.log('description: ' + body.description, 'length: ' + courses.length);

    body.id = courseNextId;
    courses.push(body);

    res.json(body);

});

app.delete('/courses/:id', (req, res) => {
    let courseId = Number(req.params.id);
    let matchedCourse = courses.filter((course) => course.id === courseId)[0];

    if(matchedCourse) {
        courses.splice(courses.indexOf(matchedCourse), 1);
        res.json(matchedCourse);
    } else {
        res.status(404).send({'error': 'no course with such id'});
    }
});

app.put('/courses/:id', (req, res) => {
    let body = req.body;
    let courseId = Number(req.params.id);
    let matchedCourse = courses.filter((course) => course.id === courseId)[0];
    
    if(matchedCourse) {
        Object.assign(matchedCourse, body);
        res.json(matchedCourse);
    } else {
        res.status(404).send({'error': 'no course with such id'});
    }


});

app.use(express.static(__dirname + '/public'));

const server = app.listen(port, () => {

  const host = server.address().address;
  console.log("Express server listening at http://%s:%s\nRunning on Node %s", host, port, process.version);

});
